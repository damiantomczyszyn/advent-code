file1 = open('input4.txt', 'r')
Lines = file1.readlines()
  
#print(len(Lines))
listA = []
listB = []
listA2 = []
listB2 = []
test = True
list_of_elfs_total_kcal=[]
i=0
for line in Lines:
    line=line.strip()
    line=line.split(",")
    #print(line)
    for x in line:
        x=x.split("-")    
        #print(x)    
        if test == False:
            listA.append(int(x[0]))
            listB.append(int(x[1]))

            test = True
            break        
        listA2.append(int(x[0]))
        listB2.append(int(x[1]))
        
        test = False
        
'''
print(listA)
print()
print(listB)
print()
print(listA2)
print()
print(listB2)
'''

list_range1 = []
list_range2 = []
dl=(len(listA))

for x in range(dl):
    templist = []
    for y in range(listA.pop(),listB.pop()+1):
        templist.append(y)    
    list_range1.append(templist)
    templist = []
    for y in range(listA2.pop(),listB2.pop()+1):
        templist.append(y)    
    list_range2.append(templist)


count=0
for i in range(dl):
    if set(list_range1[i]).issubset((set(list_range2[i]))) or set(list_range2[i]).issubset(set(list_range1[i]))  :
        count+=1

print(count)

#part 2

count=0
for i in range(dl):
    if set(list_range1[i]).intersection((set(list_range2[i]))) or set(list_range2[i]).intersection(set(list_range1[i]))  :
        count+=1

print(count)